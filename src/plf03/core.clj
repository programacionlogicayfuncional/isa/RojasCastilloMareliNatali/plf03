(ns plf03.core)

;;Bloque de funciones para "comp"

(defn función-comp-1
  []
  (let [ f (fn [x] inc x)
        g (fn [x] (* 3 x))
        z (comp f g)]
    (z 4)))

(defn función-comp-2
  []
  (let [f (fn [x] (mapv - [4] x))
        g (fn [xs] (filter even? xs))
        h(fn [x xs] (mapv + x xs))
        z (comp f g h)]
    (z[ 2] [4 4 67 -3 5 6] )))

(defn función-comp-3
  []
  (let [f (fn [x] (dec x))
        g (fn [x y] (* x y))
        z (comp f g)]
    (z 4 7)))

(defn función-comp-4
  []
  (let [f (fn [x] (filter even? x))
        g (fn [xs] (map dec xs))
        h (fn [x xs] (map + xs x))
        z (comp f g h)]
    (z [4 3 2] [4 3 67 -3 5 6])))

(defn función-comp-5
  []
  (let [f (fn [x] (map-indexed vector x) )
        g (fn [x] (reverse x))
        z (comp g f)]
    (z "Maar")))

(defn función-comp-6
  []
  (let [f (fn [xs] (mapv - [4] xs))
        g (fn [ys] (filter even? ys))
        i (fn [xs ys ws] (mapv + xs ys ws))
        z (comp f g i)]
    (z [2 3 4 5] [4 4 67 -3] [55 4 66 8])))


(defn función-comp-7
  []
  (let [f (fn [x] (group-by flatten x))
        g (fn [x y] (merge-with x y))
        z (comp f g)]
    (z '(10 20 [3 (4 5)]) {:k1 900  :k2 98 :k3 456})))

(defn función-comp-8
  []
  (let [f (fn [x] (mapv - [4] x))
        g (fn [xs] (filter even? xs))
        h (fn [x xs] (mapv + x xs))
        z (comp f g h)]
    (z [2 3 4] [3 6 20 4 5])))

(defn función-comp-9
  []
  (let [f (fn [x] (inc x))
        g (fn [x y] (* x y))
        z (comp f g)]
    (z 4 7)))

(defn función-comp-10
  []
  (let [f (fn [x] (shuffle x))
        g (fn [xs] (subvec xs 2))
        h (fn [x xs] (conj x xs))
        z (comp f g h)]
    (z [4 3 2] [4 3 67 -3 5 6])))

(defn función-comp-11
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (true? x))
        z (comp g f)]
    (z  [9 8 7 6])))

(defn función-comp-12
  []
  (let [f (fn [x] (mapv dec x))
        g (fn [xs] (split-at 3 xs))
        h (fn [xs x] (into x xs))
        z (comp g f h)]
    (z  [45] [9 8 7 6 8356 34 23 2])))

(defn función-comp-13
  []
  (let [g (fn [xs] (split-at 1 xs))
        h (fn [xs x] (replace x xs))
        z (comp g h)]
    (z  [:uno :dos :tres] [0 1 2])))

(defn función-comp-14
  []
  (let [f (fn [x] (mapv inc x))
        h (fn [xs x] (mapcat list xs x))
        z (comp f h)]
    (z [3 4 5 7 8] [1 2 3 4])))

(defn función-comp-15
  []
  (let [f (fn [x] (split-with odd? x))
        g (fn [xs] (update xs 0 dec))
        h (fn [xs x] (into xs x))
        z (comp f g h)]
    (z [1 3 5 6 7 9] [23 2 33])))

(defn función-comp-16
  []
  (let [f (fn [x] (group-by flatten x))
        h (fn [xs x] (merge-with xs x))
        z (comp f h)]
    (z '(10 20 [3 (4 5)]) {:key1 23 :key2 87 :key3 21})))

(defn función-comp-17
  []
  (let [f (fn [x] (reverse x))
        h (fn [xs x] (concat xs x))
        z (comp f h)]
    (z "Mareli" {:key1 23 :key2 87 :key3 21})))


(defn función-comp-18
  []
  (let [f (fn [x] (first x))
        h (fn [xs x] (zipmap x xs))
        z (comp f h)]
    (z {:uno :dos :tres :cuatro} [1 2 3])))

(defn función-comp-19
  []
  (let [f (fn [x] (* x))
        h (fn [xs x] (+ x xs))
        z (comp f h)]
    (z 4 5)))

(defn función-comp-20
  []
  (let [f (fn [x] (string? x))
        i (fn [xs] (reverse xs))
        h (fn [x xs] (str x xs))
        z (comp f i h)]
    (z 2 "saerat")))
    


(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7) 
(función-comp-8) 
(función-comp-9)
(función-comp-10) 
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

;;Bloque de funciones para "complement"

(defn función-complement-1
  []
  (let [f (fn [x]( pos? x))
        z (complement f)]
    (z 4)))

(defn función-complement-2
  []
  (let [f (fn [x] (nat-int?  x))
        z (complement f)]
    (z '(-890 890))))

(defn función-complement-3
  []
  (let [f (fn [x] (ratio? x))
        z (complement f)]
    (z 4657)))

(defn función-complement-4
  []
  (let [f (fn [x] (boolean?  x))
        z (complement f)]
    (z (< 2 3))))

(defn función-complement-5
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z #{4 5 9 7 6})))


(defn función-complement-6
  []
  (let [f (fn [x] (coll?  x))
        z (complement f)]
    (z "NATALI")))

(defn función-complement-7
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z [{3 4 5 6} [33 44] \s])))


(defn función-complement-8
  []
  (let [f (fn [x] (float?  x))
        z (complement f)]
    (z 2345.3333333333333333333)))


(defn función-complement-9
  []
  (let [f (fn [x] (indexed? x))
        z (complement f)]
    (z ['(9 2 3) #{true false} [1 2 3]])))


(defn función-complement-10
  []
  (let [f (fn [x] (keyword?  x))
        z (complement f)]
    (z [:a :b :c :d])))

(defn función-complement-11
  []
  (let [f (fn [x] (range x))
        z (complement f)]
    (z 19)))

(defn función-complement-12
  []
  (let [f (fn [x] (ratio? x))
        z (complement f)]
    (z 348)))

(defn función-complement-13
  []
  (let [f (fn [x] (integer? x))
        z (complement f)]
    (z 19.90)))

(defn función-complement-14
  []
  (let [f (fn [x] (seq? x))
        z (complement f)]
    (z #{\a 'd :2})))

(defn función-complement-15
  []
  (let [f (fn [x] (symbol? x))
        z (complement f)]
    (z 'f)))

(defn función-complement-16
  []
  (let [f (fn [x] (< x 345))
        z (complement f)]
    (z 34)))

(defn función-complement-17
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z \t)))

(defn función-complement-18
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z "Tarea plf03")))

(defn función-complement-19
  []
  (let [f (fn [x] (some? x))
        z (complement f)]
    (z (list 9 8 7 6))))

(defn función-complement-20
  []
  (let [f (fn [x] (nil? x))
        z (complement f)]
    (z nil)))


(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-5)
(función-complement-4)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;;Bloque de funciones para "constantly"

(defn función-constantly-1
  []
  (let [x '(3 4 6)
        z (constantly x)]
    (z 4)))

(defn función-constantly-2
  []
  (let [x [\a \b \c \d]
        z (constantly x)]
    (z '(-890 890))))

(defn función-constantly-3
  []
  (let [x ["si" "se" "puede"]
        z (constantly x)]
    (z 4)))

(defn función-constantly-4
  []
  (let [x '(\s \f \g)
        z (constantly x)]
    (z {2 3 4 5} 3456 [[3] [5 6 7] [22 3]])))


(defn función-constantly-5
  []
  (let [x {:k 'p :j 'l :ñ 'F}
        z (constantly x)]
    (z "Materia")))


(defn función-constantly-6
  []
  (let [x 21
        z (constantly x)]
    (z 'Edad)))

(defn función-constantly-7
  []
  (let [x  #{"Mareli" "Rojas" "Castillo"}
        z (constantly x)]
    (z #{"Nombre" "ap1" "ap2"})))

(defn función-constantly-8
  []
  (let [x nil
        z (constantly x)]
    (z (list 9 8 7 6))))


(defn función-constantly-9
  []
  (let [x {'p 'r 'a 'c 't 'i 'c 'a}
        z (constantly x)]
    (z 'trabajo)))


(defn función-constantly-10
  []
  (let [x (hash-map \p 4 \l 5 \j 3)
        z (constantly x)]
    (z (list true false) 234 [\a \c \b] #{'r 't''y})))

(defn función-constantly-11
  []
  (let [x "FUNCIÓN CONSTANTLY"
        z (constantly x)]
    (z \t 1234 [2 3 4] {:key1 [2 34]:key2 [9 8 7] :key3 [\s \f]}))) 


(defn función-constantly-12
  []
  (let [x (list 9 8 7 6)
        z (constantly x)]
    (z 'ar \d [3 4 5] #{true false})))


(defn función-constantly-13
  []
  (let [x []
        z (constantly x)]
    (z (range 2 3))))


(defn función-constantly-14
  []
  (let [x (vector 899 566 345 22 1)
        z (constantly x)]
    (z "prueba" "03")))
 
(defn función-constantly-15
  []
  (let [x #{'a 'b 'c 'd}
        z (constantly x)]
    (z ["resultado" 15])))

(defn función-constantly-16
  []
  (let [x "semestre 7"
        z (constantly x)]
    (z 'semestre )))

(defn función-constantly-17
  []
  (let [x  [#{9 8 7 6} '(\a \b \c) {true false}]
        z (constantly x)]
  (z [3 4 56] (list 'v 'd 's 's) 234 #{})))

(defn función-constantly-18
  []
  (let [x (vector "Estudiante" "semestre" "año")
        z (constantly x)]
    (z 894584985945)))


(defn función-constantly-19
  []
  (let [x {'p 'r 'a 'c 't 'i 'c 'a [8 9 7 6] [2 3]}
        z (constantly x)]
    (z 'nargs)))


(defn función-constantly-20
  []
(let [x (vector \c \o \n \s \t \a \n \t \l \y)
        z (constantly x)]
    (z (vector true false) 234 [\a \c \b] #{'r 't''y})))


(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

;;Bloque de funciones para "every-pred"

(defn función-every-pred-1
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (true? x))
        h (fn [x] (boolean? x))
        z (every-pred f g h)]
    (z 4)))

(defn función-every-pred-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (even? x))
        h (fn [x] (nat-int? x))
        z (every-pred f g h)]
    (z 412)))

(defn función-every-pred-3
  []
  (let [f (fn [x] (associative? x))
        g (fn [x] (coll? x))
        h (fn [x] (indexed? x))
        z (every-pred f g h)]
    (z [1 2 3 4])))

(defn función-every-pred-4
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (double? x))
        h (fn [x] (number? x))
        z (every-pred f g h)]
    (z 523.34)))

(defn función-every-pred-5
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (number? x))
        h (fn [x] (pos? x))
        z (every-pred f g h)]
    (z "nat")))

(defn función-every-pred-6
  []
  (let [f (fn [x] (map-entry? x))
        g (fn [x] (coll? x))
        h (fn [x] (map? x))
        i (fn [x] (seq? x))
        z (every-pred f g h i)]
    (z {:a 1 :b 2 :c 3})))

(defn función-every-pred-7
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (sequential? x))
        h (fn [x] (some? x))
        i (fn [x] (coll? x))
        j (fn [x] (associative? x))
        z (every-pred f g h i j)]
    (z [[23 4 56] [2 4] [9 8 7 6]])))


(defn función-every-pred-8
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (ratio? x))
        h (fn [x] (pos? x))
        i (fn [x] (float? x))
        z (every-pred f g h i)]
    (z 8/9)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? (first x)))
        h (fn [x] (seqable? x))
        z (every-pred f g h)]
    (z "PLF")))


(defn función-every-pred-10
  []
  (let [f (fn [x] (pos-int? x))
        g (fn [x] (number? x))
        h (fn [x] (nat-int? x))
        i (fn [x] (int? x))
        z (every-pred f g h i)]
    (z 347563)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (true? x))
        z (every-pred f g)]
    (z (< 3 4))))

(defn función-every-pred-12
  []
  (let [f (fn [x] (ratio? x))
        z (every-pred f )]
    (z 4.345)))

(defn función-every-pred-13
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (integer? x))
        h (fn [x] (pos? x))
        z (every-pred f g h)]
    (z 43455N)))


(defn función-every-pred-14
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (odd? x))
        h (fn [x] (nat-int? x))
        z (every-pred f g h)]
    (z -3345)))

(defn función-every-pred-15
  []
  (let [f (fn [x] (odd? x))
        z (every-pred f)]
    (z 8774532)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (keyword? x))
        z (every-pred f)]
    (z :-)))

(defn función-every-pred-17
  []
  (let [g (fn [x] (odd? x))
        h (fn [x] (number? x))
        z (every-pred g h)]
    (z 378)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (rational? x))
        h (fn [x] (pos? x))
        z (every-pred f g h)]
    (z 1.0M)))


(defn función-every-pred-19
  []
  (let [f (fn [x] (indexed? x))
        g (fn [x] (coll? x))
        h (fn [x] (vector? x))
        i (fn [x] (seq? x))
        z (every-pred f g h i)]
    (z [\a \b \c \d])))

(defn función-every-pred-20
  []
  (let [f (fn [x] (integer? x))
        z (every-pred f)]
    (z '(3 4 5))))


(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

;;Bloque de funciones para "fnil"

(defn función-fnil-1
  []
  (let [ h (fn [x y] (if (== x y) (max x y)(min x y)))
        z (fnil h 2)]
    (z nil 8)))

(defn función-fnil-2
  []
  (let [f (fn [xs ys] (if (coll? xs) (conj xs ys) (drop-last xs)))
        z (fnil f [1 2  5 6 8])]
    (z nil [2 3 4])))


(defn función-fnil-3
  []
  (let [h (fn [x y z] (cond
                        (== x y z) (str "los números son:" "iguales")
                        (> x y) (if (> x z) (str "El número mayor: " x) (str "el mayor es:" z))
                        (> y z) (if (> y z) (str "El número mayor: " y) (str "el mayor es:" z))) )
        z (fnil h 9)]
    (z  nil 8 10)))

(defn función-fnil-4
  []
  (let [f (fn [x y] ( if (< x y)(rem x y )(str x y)))
        z (fnil f 3.5)]
    (z nil 2.9)))

(defn función-fnil-5
  []
  (let [h (fn [x y] (if (list? x) (cons 5 x) (if (number? y)(inc y)())))
        z (fnil h '(8 7 6 9 2 1))]
    (z nil 3)))

(defn función-fnil-6
  []
  (let [f (fn [xs ys] (if (map? xs)(into xs ys) ()))
        z (fnil f {:4 4})]
    (z nil {:1 1})))

(defn función-fnil-7
  []
  (let [f (fn [xs x] (if (seq? xs) (repeat x xs) (str "no es" "seq")))
        z (fnil f '(1 2 3 4 5 6))]
    (z nil 3)))

(defn función-fnil-8
  []
  (let [f (fn [xs ys] (cond (map? xs)(if (vector? ys)(replace xs ys)(empty ys))))
        z (fnil f {2 :a, 4 :b})]
    (z nil [1 2 3 4])))


(defn función-fnil-9
  []
  (let [h (fn [xs x] (if (vector? xs) (split-at x xs) (empty xs)))
        z (fnil h [38 94 73 95])]
    (z nil 3)))

(defn función-fnil-10
  []
  (let [g (fn [xs x] (if (string? xs) (str xs x)(str x)))
        z (fnil g "Espero ")]
    (z nil "comprender esta función")))

(defn función-fnil-11
  []
  (let [f (fn [xs ys] (if (map? xs)(if (map? ys) (merge xs ys) (str "no es mapa" ys))(empty xs)))
        z (fnil f {:nombre "Natali"})]
    (z nil {:ap1 "Cast"})))

(defn función-fnil-12
  []
  (let [f (fn [x y] (if (< y x) (* y x) (- x y)))
        z (fnil f 23N)]
    (z nil 2N)))

(defn función-fnil-13
  []
  (let [h (fn [x y] (if (> y x) (range x y)(range y x 5)))
        z (fnil h 890)]
    (z nil 915)))

(defn función-fnil-14
  []
  (let [f (fn [x y ] (if (> y x) (quot y x) (+ x y)))
        z (fnil f 2)]
    (z nil 7)))


(defn función-fnil-15
  []
  (let [f (fn [xs x] (if (string? xs) (subs xs x)(str "" x)))
        z (fnil f "Mareli Natali Rojas")]
    (z nil 7)))


(defn función-fnil-16
  []6
  (let [f (fn [xs xy] (if (number? xs )(if (number? xy) (range xs xy) (str " " xy))()))
        z (fnil f 2)]
    (z nil 19)))

(defn función-fnil-17
  []
  (let [h (fn [xs x] (if (vector? x) (take x xs)(sort xs)))
        z (fnil h [9 8 7 6 5 4 3 2 1])]
    (z nil 4)))

(defn función-fnil-18
  []
  (let [f (fn [xs k] (if (map? xs) (select-keys xs k)(str "no es mapa")))
        z (fnil f {:grupo 'B :semestre 7})]
    (z nil [:grupo])))

(defn función-fnil-19
  []
  (let [h (fn [xs xy] (if (map? xs) 
                        (if (vector? xy) (select-keys xs xy) (str "debe ser vector"))
                        ()))
        z (fnil h {:a \z :b \y :c \x})]
    (z nil [:b])))

(defn función-fnil-20
  []
  (let [f (fn [xs ys] (if (vector? xs) (zipmap xs ys) (str "no hacer nada")))
        z (fnil f [199 299 399 499 599])]
    (z nil [\a \b \c])))


(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)




;;Bloque de funciones para "juxt"

(defn función-juxt-1
  []
  (let [f (fn [x] (rest x))
        g (fn [x] (coll? x))
        z (juxt f g)]
    (z '(:a :b :c :d))))

(defn función-juxt-2
  []
  (let [f (fn [x] (concat x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "Rojas" )))


(defn función-juxt-3
  []
  (let [
        f (fn [x] (first x))
        g (fn [x] (flatten x))
        z (juxt f g )]
    (z {:n "cdmx" :num 123})))


(defn función-juxt-4
  []
  (let [f (fn [x] (drop-last 1 x))
        g (fn [x] (distinct x))
        h (fn [x] (filter even? x))
        z (juxt f g h)]
    (z [1 2 3 4 4 4 3 5])))

(defn función-juxt-5
  []
  (let [f (fn [x] (dissoc x :a :b))
        g (fn [x] (contains? x :d))
        i (fn [x] (get x :f))
        z (juxt f g i)]
    (z {:a :c :e :d :f \n :g \f} )))

(defn función-juxt-6
  []
  (let [f (fn [x] (drop 1 x))
        h (fn [x] (group-by count x))
        g (fn [x] (drop-last 1 x))
        i (fn [x] (filter vector? x))
        z (juxt f h g i)]
    (z [['s 'd 'f] ["nombres" "apellidos"] [true false] [1 2 3 4]])))

(defn función-juxt-7
  []
  (let [f (fn [x] (group-by flatten x))
        h (fn [x] (reverse x))
        z (juxt f h)]
    (z '(10 20 [3 (4 5)]))))

(defn función-juxt-8
  []
  (let [f (fn [x] (string? x))
        h (fn [x] (seqable? x))
        g (fn [x] (reverse x))
        z (juxt f h g)]
    (z "clojure")))

(defn función-juxt-9
  []
  (let [f (fn [x] (drop-while neg? x))
        h (fn [x] (take 2 x))
        g (fn [x] (frequencies x))
        i (fn [x] (flatten x))
        z (juxt f h g i)]
    (z #{1 2 3 9 8 7})))


(defn función-juxt-10
  []
  (let [f (fn [x] (cons 5 x))
        z (juxt f )]
    (z '(\v \d \t \s))))

(defn función-juxt-11
  []
  (let [f (fn [x] (some even? x))
        g (fn [x] (take 3 x))
        z (juxt f g)]
    (z '(3 27 5 9 17 3 57 23 1 2 9 12 15))))

(defn función-juxt-12
  []
  (let [f (fn [x] (some int? x))
        g (fn [x] (drop 1 x))
        h (fn [x] (filterv pos? x))
        z (juxt f g h)]
    (z [0.5 -3 5.4 -97 0.4M])))

(defn función-juxt-13
  []
  (let [f (fn [x] (concat x))
        g (fn [x] (repeat 2 x))
        h (fn [x] (subs x 8))
        z (juxt f g h)]
    (z "Materia PLF")))


(defn función-juxt-14
  []
  (let [f (fn [x] (sort-by first > x))
        g (fn [x] (take 1 x))
        h (fn [x] (filterv vector? x))
        i (fn [x] (keep int? x))
        z (juxt f g h i)]
    (z [[1 2] [2 2] [2 3]])))

(defn función-juxt-15
  []
  (let [f (fn [x] (keep indexed? x))
        z (juxt f)]
    (z '('() [] #{} {}))))

(defn función-juxt-16
  []
  (let [f (fn [x y] (keep x y))
        h (fn [x y] (replace x y))
        z (juxt f h)]
    (z {:a 1, :b 2, :c 3} [:a :b :d])))

(defn función-juxt-17
  []
  (let [f (fn [x y] (keep x y))
        h (fn [x y] (replace x y))
        z (juxt f h)]
    (z {:k1 \a :k2 \b :k3 \c} [:a :b :d])))

(defn función-juxt-18
  []
  (let [f (fn [x] (update x 2 inc))
        h (fn [x] (take 3 x))
        i (fn [x] (split-with odd? x))
        z (juxt f h i)]
    (z [1 2 3])))


(defn función-juxt-19
  []
  (let [f (fn [x] (sort-by count x))
        h (fn [x] (reduce-kv conj [] x))
        z (juxt f h)]
    (z ["aaa" "bb" "c"])))


(defn función-juxt-20
  []
  (let [f (fn [x y] (keep x y))
        h (fn [x y] (replace x y))
        z (juxt f h)]
    (z {:k1 [1 2 3 4] :k2 [9 8 7] :k3 [5 6 7]} [4 5 6])))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-14)
(función-juxt-13)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)









;;Bloque de funciones para "partial"

(defn función-partial-1
  []
  (let [
        g (fn [x y] (vector x y))
        z (partial  g 2)]
    (z 6)))

(defn función-partial-2
  []
  (let [
        h (fn [x y] (range x y))
        z (partial h 2 )]
    (z 8)))

(defn función-partial-3
  []
  (let [g (fn [x y w] (list x y w))
        z (partial  g 3 '(\a \b \c))]
    (z [3 4 5])))

(defn función-partial-4
  []
  (let [g (fn [x y w v] (concat x y w v))
        z (partial  g "Mareli" "Natali")]
    (z "Rojas" "Castillo")))

(defn función-partial-5
  []
  (let [g (fn [x y w v] (distinct? x y w v))
        z (partial  g 1)]
    (z 2 3 3)))

(defn función-partial-6
  []
  (let [g (fn [a b c d e f g] (max a b c d e f g))
        z (partial  g 14 55)]
    (z 12 176 234 23 35)))

(defn función-partial-7
  []
  (let [g (fn [a b c d e f g h i j k] (min a b c d e f g h i j k))
        z (partial  g 14 55 34 7)]
    (z 12 176 234 23 35 -23 -124)))

(defn función-partial-8
  []
  (let [g (fn [a b c d] (str a b c d))
        z (partial  g "sistemas")]
    (z "Comp" 'tarea :plf03)))

(defn función-partial-9
  []
  (let [g (fn [a b c] (mapv + a b c))
        z (partial  g [8 7 6] [2 3])]
    (z [1 11 9])))

(defn función-partial-10
  []
  (let [g (fn [a b c] (mapv - a b c))
        z (partial  g [8 7 6] [2 3])]
    (z [1 11 9])))

(defn función-partial-11
  []
  (let [g (fn [x y] (drop x y))
        z (partial g 2)]
    (z [1 11 9])))

(defn función-partial-12
  []
  (let [g (fn [x y] (find x y))
        z (partial :a g)]
    (z {:a '(3 4 5) :b \r :c [true false] :d #{'r 't 'y}})))

(defn función-partial-13
  []
  (let [g (fn [x y w v] (frequencies [x y w v]))
        z (partial g :a \a)]
    (z :a :a)))

(defn función-partial-14
  []
  (let [g (fn [x y] (into x y))
        z (partial g {:2 43})]
    (z {:1 1})))

(defn función-partial-15
  []
  (let [g (fn [x y w v z] (merge x y w v z))
        z (partial g {:k2 43} {:k5 true})]
    (z {:k1 1} {:k3 15} {:k4 20})))

(defn función-partial-16
  []
  (let [g (fn [x y w ] (partition x y (range w )))
        z (partial g 2 3)]
    (z 10)))

(defn función-partial-17
  []
  (let [g (fn [x y] (quot x y))
        z (partial g 50 )]
    (z 2)))

(defn función-partial-18
  []
  (let [g (fn [x y] (replace x y))
        z (partial g ["m" "a" "r"])]
    (z [2])))

(defn función-partial-19
  []
  (let [g (fn [x y] (split-at x y))
        z (partial g 3)]
    (z [2 87 462 1 4 99 33])))

(defn función-partial-20
  []
  (let [g (fn [x y w] (subs x y w))
        z (partial g "alumna de sistemas")]
    (z 10 16)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

;;Bloque de funciones para "some-fn"

(defn función-some-fn-1
  []
  (let [g (fn [x] (pos? x))
        h (fn [x] (ratio? x))
        z (some-fn g h)]
    (z 6)))

(defn función-some-fn-2
  []
  (let [h (fn [x] (coll? x))
        f (fn [x] (> 3 x))
        z (some-fn h f)]
    (z 67)))

(defn función-some-fn-3
  []
  (let [f (fn [xs] (associative? xs))
        h (fn [xs] (boolean? xs))
        g (fn [xs] (indexed? xs ))
        z (some-fn h f g)]
    (z [4 5 23 8 9 11 12])))

(defn función-some-fn-4
  []
  (let [f (fn [x] (list? x))
        h (fn [x] (seq? x))
        z (some-fn f h)]
    (z '(8 78 6 12))))

(defn función-some-fn-5
  []
  (let [f (fn [x] (set? x))
        g (fn [x] (some? x))
        h  (fn [x] (sequential? x))
        i (fn [x] (seqable? x))
        z (some-fn f g h i)]
    (z #{1 2 3})))

(defn función-some-fn-6
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z :key)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? x))
        i (fn [x] (map? x))
        z (some-fn f g i)]
    (z 22/7)))

(función-some-fn-7)

(defn función-some-fn-8
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z (hash-map :a 1 :b 2))))

(defn función-some-fn-9
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (number? x))
        i (fn [x] (integer? x))
        z (some-fn f g i)]
    (z 33N)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (seq? x))
        h (fn [x] (integer? x))
        i (fn [x] (float? x))
        z (some-fn f g h i)]
    (z 150.50)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (string? x))
        z (some-fn f)]
    (z "Función")))

(defn función-some-fn-12
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (string? x))
        z (some-fn f g)]
    (z :a)))

(defn función-some-fn-13
  []
  (let [f (fn [x] (int? x))
        h (fn [x] (list? x))
        g (fn [x] (pos-int? x))
        z (some-fn f h g)]
    (z (vector \c \f \t \b))))

(defn función-some-fn-14
  []
  (let [f (fn [x] (int? x))
        h (fn [x] (char? x))
        g (fn [x] (ratio? x))
        i (fn [x] (seq? x))
        z (some-fn f h g i)]
    (z (> 9 8))))


(defn función-some-fn-15
  []
  (let [f (fn [x] (string? x))
        h (fn [x] (char? x))
        g (fn [x] (ratio? x))
        z (some-fn f h g)]
    (z 100)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (symbol? x))
        h (fn [x] (boolean? x))
        g (fn [x] (ratio? x))
        z (some-fn f h g)]
    (z {:c1 100 :c2 200})))

(defn función-some-fn-17
  []
  (let [f (fn [x] (number? x))
        h (fn [x] (sequential? x))
        z (some-fn f h)]
    (z (range 1 5))))

(defn función-some-fn-18
  []
  (let [f (fn [x] (float? x))
        h (fn [x] (symbol? x))
        i (fn [x] (some? x))
        j (fn [x] (nil? x))
        z (some-fn f h i j)]
    (z 'N)))


(defn función-some-fn-19
  []
  (let [f (fn [x] (keyword? x))
        h (fn [x] (string? x))
        i (fn [x] (symbol? x))
        z (some-fn f h i)]
    (z "Nombre y apellidos")))


(defn función-some-fn-20
  []
  (let [f (fn [x] (ratio? x))
        h (fn [x] (number? x))
        z (some-fn f h)]
    (z [[1 2 3] [\a \c]])))



(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-9)
(función-some-fn-8)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)

